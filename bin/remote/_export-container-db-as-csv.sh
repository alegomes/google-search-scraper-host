#!/bin/bash

LASTDBBKP=/tmp/lastbkpdb

TIMESTAMP=$(date +%Y%m%d%H%M)
BKP_FILE=/tmp/search_results_$TIMESTAMP.csv
CMD_SQL="copy search_results to '$BKP_FILE'  DELIMITER ',' CSV HEADER"
CONTAINER_ID=$(docker container ls -a | grep postgres | cut -d ' ' -f1)
PSQL="psql -U postgres -d googlescraper -c \"$CMD_SQL\""
DOCKER="docker exec -u 0 $CONTAINER_ID $PSQL"

bash -c "$DOCKER"

RESULT=$?
if [ $RESULT -ne 0 ]; then
	echo
	echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo "!!!!!!!!!! BACKUP FAILED !!!!!!!!!!"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    exit -1
else
	echo
	echo -n $BKP_FILE > $LASTDBBKP
fi
