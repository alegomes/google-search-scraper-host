#!/bin/bash

CONTAINER_ID=$(docker container ls -a | grep postgres | cut -d ' ' -f1)

echo "PostreSQL container is $CONTAINER_ID"

echo "Moving $1 to $2 in container $CONTAINER_ID"

DOCKER_CMD="docker exec -u 0 $CONTAINER_ID mv $1 $2"

echo "Docker command to be executed is: $DOCKER_CMD"

bash -c "$DOCKER_CMD"

echo "Files moved."