#!/bin/bash

# DEFAULTS: sudo bin/run.sh -u input/2019.Argentina/users-prod.csv -d $(pwd)

function check_args {
	if [ -z $1 ] || [ -z $2 ]; then
		usage
	fi
}

function run_interactive {
	echo 'Running container in interactive mode... '$@
	# check_args $@
	# docker run --rm -e "users=$1" --mount output:${2}/output -v input:${2}/input,readonly -it alegomes/google-search-scraper /bin/bash	
	# docker run  --rm --restart=always --mount type=bind,source=/home/linuxadmin/output/,target=/app/output --mount type=bind,source=/home/linuxadmin/input/,target=/app/input -d alegomes/google-search-scraper 
	# docker run  --rm --mount type=bind,source=/home/linuxadmin/scraper/output/,target=/scraper/output --mount type=bind,source=/home/linuxadmin/scraper/input/,target=/scraper/input -it alegomes/google-search-scraper /bin/bash
	# docker run  --rm --mount type=bind,source=/root/google-search-scraper-host/output/,target=/scraper/output --mount type=bind,source=/root/google-search-scraper-host/input,target=/scraper/input -it alegomes/google-search-scraper /bin/bash
	docker run  --rm --mount type=bind,source=/home/linuxadmin/google-search-scraper-host/output/,target=/scraper/output --mount type=bind,source=/home/linuxadmin/google-search-scraper-host/input,target=/scraper/input -it alegomes/google-search-scraper /bin/bash
}

function run_daemon {
	echo 'Running container in daemon mode...' $@
	# check_args $@
	# docker run -e "users=$1" -v output:${2}/output -v input:${2}/input,readonly -d alegomes/google-search-scraper	
	docker run  --restart=always --mount type=bind,source=/home/linuxadmin/google-search-scraper-host/output/,target=/scraper/output --mount type=bind,source=/home/linuxadmin/google-search-scraper-host/input,target=/scraper/input alegomes/google-search-scraper

}

function usage {
	echo
	echo "Usage: $0 [-i] -u <users.csv> -d <dir>"
	echo
	echo "   -i For interactive mode"
	echo "   -u Users csv file (users_test.csv or users_prod.csv)"
	echo "   -d Local base dir to mount in the cotainer"
	echo
	echo " e.g. When running locally:       $0 -u input/users-test.csv -d $(pwd)"
	echo "      When running interactively: $0 -u input/users-test.csv -d $(pwd) -i"
	echo "      When running remotelly:     $0 -u input/users-prod.csv -d /home/linuxadmin"
	echo
	exit -1 
}

###### Start

# if [ $# -lt 1 ]; then
# 	usage
# fi

# home=$1

# mkdir -p /home/linuxadmin/scraper/{input,output}

while getopts "i" OPT
do
  case $OPT in
    i) 
		# INTERTACTIVE=True
		run_interactive
		;;
	# u) 
	# 	USERS=$OPTARG
	# 	;;
	# d)
	# 	DIR=$OPTARG
	# 	;;
	*)
		run_daemon
		;;
	\?) 
		usage
		;;
  esac
done

# shift $((OPTIND - 1))
# ARG1=${@:$OPTIND:1}
# ARG2=${@:$OPTIND+1:1}


# if [ -z $USERS ]; then
# 	echo "-u option required."
# 	usage
# fi

# if [ -z $DIR ]; then
# 	echo "-d option required."
# 	usage
# fi

# if [ -z $INTERACTIVE ]; then
# 	run_daemon $USERS $DIR
# else
# 	run_interactive $USERS $DIR
# fi







# home=/home/linuxadmin; docker run -v output:${home}/output -v input:${home}/input,readonly -it alegomes/google-search-trainer /bin/bash