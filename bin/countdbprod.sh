#!/bin/bash

CONTAINER_ID=$(docker container ls -a | grep postgres | cut -d ' ' -f1)

docker exec -it $CONTAINER_ID psql -U postgres -d googlescraper -c 'select count(*) from search_results'