#!/bin/bash

psql -U alegomes -d googlescraperprod -c "COPY search_results TO '$(pwd)/google_scraper_$(date +%Y%m%d%H%M).csv' DELIMITER ',' CSV HEADER;"