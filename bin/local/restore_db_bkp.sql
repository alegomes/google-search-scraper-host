
create database googlescraperprod201911131045;
create database googlescraperprodhtmls201911131813;

create table search_results (
	id serial not null unique,
	timestamp timestamp with time zone,
	account varchar not null,
	query varchar(255) not null,
	position smallint not null,
	title varchar(500) not null,
	description text null,
	url varchar(255) null,
	error text null,
	created_at timestamp with time zone not null,
	updated_at timestamp with time zone not null
);

create table search_results_db (
	id serial not null unique,
	timestamp timestamp with time zone,
	account varchar not null,
	query varchar(255) not null,
	position smallint not null,
	title varchar(500) not null,
	description text null,
	url varchar(255) null,
	error text null,
	created_at timestamp with time zone not null,
	updated_at timestamp with time zone not null
);

create table search_results_html (
	id serial not null unique,
	timestamp timestamp with time zone,
	account varchar not null,
	query varchar(255) not null,
	position smallint not null,
	title varchar(500) not null,
	description text null,
	url varchar(255) null,
	error text null,
	created_at timestamp with time zone not null,
	updated_at timestamp with time zone not null
);


-- COPY search_results(timestamp, account, query, position, title, description, url, error, created_at, updated_at) FROM PROGRAM 'sed -E "s/^[0-9]+,//g" /Users/alegomes/GDrive/2019/code/google-search-bkp/bkp/scraper/db/search_results_201911080659.csv' WITH (FORMAT CSV, HEADER);

COPY search_results_db(timestamp, account, query, position, title, description, url, error, created_at, updated_at) FROM PROGRAM 'sed -E "s/^[0-9]+,//g" /Users/alegomes/GDrive/2019/code/google-search-bkp/bkp/scraper/db/search_results_201911080659.csv' WITH (FORMAT CSV, HEADER);

# htmls

-- COPY search_results(timestamp, account, query, position, title, description, url, error, created_at, updated_at)  FROM '/Users/alegomes/code/google-search-results-html-parser/google-scraper-htmls.1911071845.csv' DELIMITER ',' CSV HEADER;
COPY search_results(timestamp, account, query, position, title, description, url, error, created_at, updated_at) FROM PROGRAM 'sed -E "s/^[0-9]+,//g" /Users/alegomes/code/google-search-results-html-parser/google-scraper-htmls.201911131806.csv' WITH (FORMAT CSV, HEADER);

COPY search_results_html(timestamp, account, query, position, title, description, url, error, created_at, updated_at) FROM PROGRAM 'sed -E "s/^[0-9]+,//g" /Users/alegomes/code/google-search-results-html-parser/google-scraper-htmls.201911131806.csv' WITH (FORMAT CSV, HEADER);

