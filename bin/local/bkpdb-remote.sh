#!/bin/bash

PG_CONTAINER_BKP_VOL=/tmp/bkpdb
REMOTE_HOST_BKP_DIR=/home/linuxadmin/google-search-scraper-host/bkp/db
LOCAL_BKP_DIR=bkp/scraper/
BKP_FILE_NAME=/tmp/lastbkpdb

echo 'Exporting database to CSV file...'

ssh -t linuxadmin@168.205.92.81 /home/linuxadmin/google-search-scraper-host/bin/remote/_export-container-db-as-csv.sh
BKP_FILE=$(ssh -t linuxadmin@168.205.92.81 cat $BKP_FILE_NAME)
BKP_FILE=${BKP_FILE%\\r}

echo "Data backed up at [$BKP_FILE]"
echo "Copying $BKP_FILE to $PG_CONTAINER_BKP_VOL"

ssh -t linuxadmin@168.205.92.81 /home/linuxadmin/google-search-scraper-host/bin/remote/_copy-from-container.sh "$BKP_FILE" $PG_CONTAINER_BKP_VOL

echo "Rsyncing from [linuxadmin@168.205.92.81:$REMOTE_HOST_BKP_DIR] to [$LOCAL_BKP_DIR]"

rsync -avH linuxadmin@168.205.92.81:$REMOTE_HOST_BKP_DIR $LOCAL_BKP_DIR