######### Total de linhas de cada tabela

googlescraperprod=# select count(*) from search_results_db;
 count
-------
 48514
(1 row)

googlescraperprod=# select count(*) from search_results_html;
 count
-------
 69549
(1 row)


######### Interseção entre A e B 

# Na unha

select h.id, d.id
from search_results_html as h,
     search_results_db as d
where h.timestamp = d.timestamp and 
      h.account = d.account and 
      h.query = d.query and
      h.position = d.position and
      h.url = d.url;

select count(*)
from search_results_html as h,
     search_results_db as d
where h.timestamp = d.timestamp and 
      h.account = d.account and 
      h.query = d.query and
      h.position = d.position and
      h.url = d.url;

 count
-------
 34701        # Mesma coisa que o INNER JOIN
(1 row)


# Com inner join

SELECT count(*) 
FROM search_results_db as d
INNER JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
   h.account = d.account and 
   h.query = d.query and
   h.position = d.position and
   h.url = d.url;

 count
-------
 34701
(1 row)


######### Linhas exclusivas de A ou 

# Left exclusive

SELECT count(*) 
FROM search_results_db as d
LEFT OUTER JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
   h.account = d.account and 
   h.query = d.query and
   h.position = d.position and
   h.url = d.url
WHERE h.id IS NULL;

 count
-------
 13813
(1 row)

# Right exclusive

SELECT count(*) 
FROM search_results_db as d
RIGHT OUTER JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
   h.account = d.account and 
   h.query = d.query and
   h.position = d.position and
   h.url = d.url
WHERE d.id IS NULL;

 count
-------
 34848
(1 row)


######### Unificando os dados numa única tabela

# DB Full + HTML Only

CREATE TABLE search_results AS (
    SELECT * FROM search_results_db
);

select count(*) from search_results;
 count
-------
 48514
(1 row)

INSERT INTO search_results (id, timestamp, account, query, position, title, description, url, error, created_at, updated_at) (
    SELECT h.id, h.timestamp, h.account, h.query, h.position, h.title, h.description, h.url, h.error, h.created_at, h.updated_at
    FROM search_results_db as d
    RIGHT JOIN search_results_html as h
    ON h.timestamp = d.timestamp and 
       h.account = d.account and 
       h.query = d.query and
       h.position = d.position and
       h.url = d.url
    WHERE d.id IS NULL
);

select count(*) from search_results;
 count
-------
 83362    # Massa. Bate com o total previsto pela planilha.
(1 row)

######### Exportar pra CSV

cd /Users/alegomes/GDrive/2019/unb/ipol/resocie/projetos/google/data/2019.Argentina
..../bin/local/export_to_csv.sh

######### Quantidade de experimentos por dia

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as date, count(distinct timestamp) from search_results group by date order by date;
    date    | count
------------+-------
 2019-10-14 |    82
 2019-10-15 |   102
 2019-10-17 |     2
 2019-10-18 |     1
 2019-10-19 |     3
 2019-10-20 |     3
 2019-10-21 |     3
 2019-10-22 |     3
 2019-10-23 |     3
 2019-10-24 |     3
 2019-10-25 |     4
 2019-10-26 |     3
 2019-10-27 |     3
 2019-10-28 |     3
 2019-10-29 |     3
 2019-10-30 |     3
 2019-10-31 |     3
 2019-11-01 |     3
 2019-11-02 |     3
 2019-11-05 |     1
 2019-11-06 |     3
 2019-11-07 |     3
            |     0
(23 rows)

######### Experimentos de um dia especifico

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as data, timestamp from search_results where to_char(timestamp :: date, 'yyyy-mm-dd') = '2019-10-25' group by data, timestamp order by data;
    data    |       timestamp
------------+------------------------
 2019-10-25 | 2019-10-25 05:00:14-03
 2019-10-25 | 2019-10-25 07:51:30-03
 2019-10-25 | 2019-10-25 10:00:06-03
 2019-10-25 | 2019-10-25 17:00:06-03
(4 rows)

######### Quantidade de contas usadas em cada experimento 

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as data, timestamp, count(distinct account)
from search_results 
where to_char(timestamp :: date, 'yyyy-mm-dd') = '2019-10-27' 
group by data, timestamp 
order by data;

    data    |       timestamp        | count
------------+------------------------+-------
 2019-10-27 | 2019-10-27 05:00:13-03 |     1
 2019-10-27 | 2019-10-27 10:00:05-03 |    15
 2019-10-27 | 2019-10-27 17:00:04-03 |    15
(3 rows)

######### Quantidade de queries realizadas por cada conta em um experimento específico

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as data, timestamp, account, count(distinct query)
from search_results 
where timestamp = '2019-10-27 10:00:05-03'
group by data, timestamp, account
order by data;
    data    |       timestamp        |     account     | count
------------+------------------------+-----------------+-------
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov     |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov2    |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov3    |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov4    |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov5    |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.neutro  |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.neutro2 |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.neutro3 |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.neutro4 |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.neutro5 |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.opo     |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.opo2    |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.opo3    |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.opo4    |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.opo5    |    14
(15 rows)

######### Queries realizadas por uma conta em um experimento específico

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as data, timestamp, account, query, count(*)
from search_results 
where timestamp = '2019-10-27 10:00:05-03' and account = 'resocie.gov'
group by data, timestamp, account, query
order by data;

    data    |       timestamp        |   account   |                query                 | count
------------+------------------------+-------------+--------------------------------------+-------
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Alberto Fernandez                    |     8
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Axel Kicilloff                       |     7
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Cristina Fernandez Kirchner          |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Horacio Rodriguez Larreta            |     8
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | José Luis Espert                     |     9
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Juan José Gómez Centurión            |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Maria Eugenia Vidal                  |    13
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Matias Lammens                       |    17
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Maurício Macri                       |    13
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Miguel Ángel Pichetto                |     9
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Nicolás del Caño                     |    14
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Roberto Lavagna                      |     8
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | elecciones presidenciales            |     9
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | encuestas elecciones presidenciales  |    10
(14 rows)

######### URLs resultantes de uma query específica

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as data, timestamp, account, query, position, url
from search_results 
where timestamp = '2019-10-27 10:00:05-03' and account = 'resocie.gov' and query = 'Alberto Fernandez'
order by position;

    data    |       timestamp        |   account   |       query       | position |                                                               url
------------+------------------------+-------------+-------------------+----------+---------------------------------------------------------------------------------------------------------------------------------
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Alberto Fernandez |        1 | https://pt.wikipedia.org/wiki/Alberto_Fern%C3%A1ndez
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Alberto Fernandez |        2 | https://es.wikipedia.org/wiki/Alberto_Fern%C3%A1ndez
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Alberto Fernandez |        3 | https://twitter.com/alferdez
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Alberto Fernandez |        4 | https://www.infobae.com/politica/2019/10/26/10-datos-que-no-sabias-de-alberto-fernandez/
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Alberto Fernandez |        5 | https://www.infobae.com/politica/2019/10/27/alberto-fernandez-no-quiere-que-nazca-el-albertismo-y-buscara-liderar-a-todo-el-pj/
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Alberto Fernandez |        6 | https://www.clarin.com/tema/alberto-fernandez.html
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Alberto Fernandez |        7 | https://www.lanacion.com.ar/tema/alberto-fernandez-tid849
 2019-10-27 | 2019-10-27 10:00:05-03 | resocie.gov | Alberto Fernandez |        8 | https://www.cronista.com/clase/dixit/Quien-es-la-novia-de-Alberto-Fernandez-pre-candidata-a-Primera-Dama-20190518-0001.html
(8 rows)

######### Quantidade de resultados por dia

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as date, count(*) from search_results group by date order by date;
    date    | count
------------+-------
 2019-10-14 |  3976
 2019-10-15 |  4824
 2019-10-17 |   224
 2019-10-18 |   113
 2019-10-19 |  1988
 2019-10-20 |  5112
 2019-10-21 |  5092
 2019-10-22 |  3535
 2019-10-23 |   342
 2019-10-24 |   321
 2019-10-25 |   355
 2019-10-26 |   481
 2019-10-27 |  4639
 2019-10-28 |  7676
 2019-10-29 |  6916
 2019-10-30 |  6834
 2019-10-31 |  6631
 2019-11-01 |  6675
 2019-11-02 |  2258
 2019-11-05 |  2339
 2019-11-06 |  6626
 2019-11-07 |  6387
            |    18
(23 rows)                # ===> Total 83.362 (YES!)

######### Quantidade de resultados por experimento

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as date, timestamp, count(*) from search_results group by date, timestamp order by date, timestamp;
    date    |       timestamp        | count
------------+------------------------+-------
 2019-10-14 | 2019-10-14 16:20:06-03 |    49
 2019-10-14 | 2019-10-14 17:00:06-03 |    49
 2019-10-14 | 2019-10-14 17:20:04-03 |    50
 2019-10-14 | 2019-10-14 17:25:04-03 |    50
 2019-10-14 | 2019-10-14 17:30:01-03 |    49
 2019-10-14 | 2019-10-14 17:35:02-03 |    50
 2019-10-14 | 2019-10-14 17:40:02-03 |    49
 2019-10-14 | 2019-10-14 17:45:02-03 |    50
 2019-10-14 | 2019-10-14 17:50:01-03 |    50
 2019-10-14 | 2019-10-14 17:55:02-03 |    50
 2019-10-14 | 2019-10-14 18:00:02-03 |    50
 2019-10-14 | 2019-10-14 18:05:02-03 |    48
 2019-10-14 | 2019-10-14 18:10:02-03 |    49
 2019-10-14 | 2019-10-14 18:15:02-03 |    50
 2019-10-14 | 2019-10-14 18:20:02-03 |    49
 2019-10-14 | 2019-10-14 18:25:02-03 |    47
 2019-10-14 | 2019-10-14 18:30:02-03 |    48
 2019-10-14 | 2019-10-14 18:35:02-03 |    47
 2019-10-14 | 2019-10-14 18:40:02-03 |    48
 2019-10-14 | 2019-10-14 18:45:02-03 |    49
 2019-10-14 | 2019-10-14 18:50:02-03 |    48
 2019-10-14 | 2019-10-14 18:55:02-03 |    49
 2019-10-14 | 2019-10-14 19:00:02-03 |    56
 2019-10-14 | 2019-10-14 19:05:02-03 |    50
 2019-10-14 | 2019-10-14 19:10:02-03 |    50
 2019-10-14 | 2019-10-14 19:15:02-03 |    50
 2019-10-14 | 2019-10-14 19:20:02-03 |    49
 2019-10-14 | 2019-10-14 19:25:02-03 |    48
 2019-10-14 | 2019-10-14 19:30:02-03 |    49
 2019-10-14 | 2019-10-14 19:35:02-03 |    48
 2019-10-14 | 2019-10-14 19:40:02-03 |    49
 2019-10-14 | 2019-10-14 19:45:02-03 |    48
 2019-10-14 | 2019-10-14 19:50:02-03 |    49
 2019-10-14 | 2019-10-14 19:55:01-03 |    49
 2019-10-14 | 2019-10-14 20:00:02-03 |    50
 2019-10-14 | 2019-10-14 20:05:02-03 |    49
 2019-10-14 | 2019-10-14 20:10:02-03 |    48
 2019-10-14 | 2019-10-14 20:15:01-03 |    48
 2019-10-14 | 2019-10-14 20:20:02-03 |    47
 2019-10-14 | 2019-10-14 20:25:02-03 |    54
 2019-10-14 | 2019-10-14 20:30:02-03 |    48
 2019-10-14 | 2019-10-14 20:35:02-03 |    50
 2019-10-14 | 2019-10-14 20:40:02-03 |    49
 2019-10-14 | 2019-10-14 20:45:01-03 |    49
 2019-10-14 | 2019-10-14 20:50:02-03 |    49
 2019-10-14 | 2019-10-14 20:55:02-03 |    48
 2019-10-14 | 2019-10-14 21:00:02-03 |    48
 2019-10-14 | 2019-10-14 21:05:02-03 |    48
 (…)


#########
#########
#########
#########
#########
#########




select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as date, count(*) from search_results group by date order by date;

Importando so o ultimo CSV

    date    | count
------------+-------
 2019-10-25 |    17
 2019-10-26 |   371
 2019-10-27 |  3645
 2019-10-28 |  5490
 2019-10-29 |  5715
 2019-10-30 |  5803
 2019-10-31 |  5783
 2019-11-01 |  5811
 2019-11-02 |  2052
            |    18
(10 rows)



Depois de importar todos os CSVs

    date    | count
------------+-------
 2019-10-11 |   146
 2019-10-14 |  3869
 2019-10-15 |  4880
 2019-10-25 |    17
 2019-10-26 |   371
 2019-10-27 |  3645
 2019-10-28 |  5490
 2019-10-29 |  5715
 2019-10-30 |  5803
 2019-10-31 |  5783
 2019-11-01 |  5811
 2019-11-02 |  2052
            |    18
(13 rows)


Importando os demais HTML

    date    | count
------------+-------
 2019-10-11 |   146
 2019-10-14 |  8418
 2019-10-15 | 10576
 2019-10-17 |   245
 2019-10-18 |   125
 2019-10-19 |  2149
 2019-10-20 |  5655
 2019-10-21 |  5720
 2019-10-22 |  3936
 2019-10-23 |   382
 2019-10-24 |   368
 2019-10-25 |   404
 2019-10-26 |   742
 2019-10-27 |  7290
 2019-10-28 | 10980
 2019-10-29 | 11430
 2019-10-30 | 11606
 2019-10-31 | 11566
 2019-11-01 | 11621
 2019-11-02 |  4104
 2019-11-05 |   689
            |    18
(22 rows)

Removendo duplicidades

select timestamp, account, query, position, url, count(*)
from search_results
group by timestamp, account, query, position, url
HAVING count(*) > 1

select timestamp, account, query, position, url from search_results where timestamp = '2019-11-01 17:00:08-03' order by account, query, position;

select count(*) from search_results;
select distinct on (timestamp, account, query, position, url) * from search_results;

delete from search_results where search_results.id not in
    (select s2.id from
        (
            select distinct on (timestamp, account, query, position, url) * from search_results
        ) as s2
    );


    date    | count
------------+-------
 2019-10-11 |   146
 2019-10-14 |  8418
 2019-10-15 | 10576
 2019-10-17 |   245
 2019-10-18 |   125
 2019-10-19 |   371
 2019-10-20 |   414
 2019-10-21 |   412
 2019-10-22 |   406
 2019-10-23 |   382
 2019-10-24 |   368
 2019-10-25 |   404
 2019-10-26 |   742
 2019-10-27 |  4026
 2019-10-28 |  5887
 2019-10-29 |  6139
 2019-10-30 |  6217
 2019-10-31 |  6187
 2019-11-01 |  6210
 2019-11-02 |  2203
 2019-11-05 |    64
            |    18
(22 rows)

Dropando tudo agora e recomeçando do zero


    date    | count
------------+-------
 2019-10-11 |   146
 2019-10-14 |  8418
 2019-10-15 | 10576
 2019-10-17 |   245
 2019-10-18 |   125
 2019-10-19 |   371
 2019-10-20 |   414
 2019-10-21 |   412
 2019-10-22 |   406
 2019-10-23 |   382
 2019-10-24 |   368
 2019-10-25 |   404
 2019-10-26 |   742
 2019-10-27 |  4026
 2019-10-28 |  5887
 2019-10-29 |  6139
 2019-10-30 |  6217
 2019-10-31 |  6187
 2019-11-01 |  6210
 2019-11-02 |  2203
 2019-11-05 |  2128
 2019-11-06 |  6292
 2019-11-07 |  6387
            |    18
(24 rows)


---------------------------------------------------------
Separacao dos dados vindos a partir do dump do banco e a partir do parse dos HTMs

# Total de linhas

googlescraperprod=# select count(*) from search_results_db;
 count
-------
 48514
(1 row)

googlescraperprod=# select count(*) from search_results_html;
 count
-------
 69549
(1 row)


# Dados salvos no banco

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as date, count(*) from search_results_db group by date order by date;

    date    | count
------------+-------
 2019-10-25 |    17
 2019-10-26 |   371
 2019-10-27 |  3645
 2019-10-28 |  5490
 2019-10-29 |  5715
 2019-10-30 |  5803
 2019-10-31 |  5783
 2019-11-01 |  5811
 2019-11-02 |  2052
 2019-11-05 |  1980
 2019-11-06 |  5873
 2019-11-07 |  5956
            |    18
(13 rows)

# Dados parseados dos HTMLs

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as date, count(*) from search_results_html group by date order by date;

    date    | count
------------+-------
 2019-10-14 |  3976
 2019-10-15 |  4824
 2019-10-17 |   224
 2019-10-18 |   113
 2019-10-19 |  1988
 2019-10-20 |  5112
 2019-10-21 |  5092
 2019-10-22 |  3535
 2019-10-23 |   342
 2019-10-24 |   321
 2019-10-25 |   347
 2019-10-26 |   337
 2019-10-27 |  3278
 2019-10-28 |  4722
 2019-10-29 |  5057
 2019-10-30 |  5194
 2019-10-31 |  5280
 2019-11-01 |  5309
 2019-11-02 |  1893
 2019-11-05 |  1773
 2019-11-06 |  5344
 2019-11-07 |  5488
(22 rows)







Agregacao do LEFT INCLUSIVE JOIN

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as date, count(*) 
from (
    SELECT h.timestamp, h.account, h.query, h.position, h.url
    FROM search_results_db as d
    LEFT JOIN search_results_html as h
    ON h.timestamp = d.timestamp and 
       h.account = d.account and 
       h.query = d.query and
       h.position = d.position and
       h.url = d.url) as li
group by date order by date;

    date    | count
------------+-------
 2019-10-25 |     9
 2019-10-26 |   227
 2019-10-27 |  2284
 2019-10-28 |  2536
 2019-10-29 |  3856
 2019-10-30 |  4163
 2019-10-31 |  4432
 2019-11-01 |  4445
 2019-11-02 |  1687
 2019-11-05 |  1414
 2019-11-06 |  4591
 2019-11-07 |  5057
            | 13813
(13 rows)

A mesma agregacao do search_results_db, so pra comparar

    date    | count
------------+-------
 2019-10-25 |    17
 2019-10-26 |   371
 2019-10-27 |  3645
 2019-10-28 |  5490
 2019-10-29 |  5715
 2019-10-30 |  5803
 2019-10-31 |  5783
 2019-11-01 |  5811
 2019-11-02 |  2052
 2019-11-05 |  1980
 2019-11-06 |  5873
 2019-11-07 |  5956
            |    18
(13 rows)

Por que o search_results_db tem mais linhas do que o OUTER LEFT INCLUSIVE?

# RIGHT OUTER JOIN
select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as date, count(*) 
from (
    SELECT h.timestamp, h.account, h.query, h.position, h.url
    FROM search_results_db as d
    RIGHT JOIN search_results_html as h
    ON h.timestamp = d.timestamp and 
       h.account = d.account and 
       h.query = d.query and
       h.position = d.position and
       h.url = d.url
    ) as ri
group by date order by date;

   date    | count
------------+-------
 2019-10-14 |  3976
 2019-10-15 |  4824
 2019-10-17 |   224
 2019-10-18 |   113
 2019-10-19 |  1988
 2019-10-20 |  5112
 2019-10-21 |  5092
 2019-10-22 |  3535
 2019-10-23 |   342
 2019-10-24 |   321
 2019-10-25 |   347
 2019-10-26 |   337
 2019-10-27 |  3278
 2019-10-28 |  4722
 2019-10-29 |  5057
 2019-10-30 |  5194
 2019-10-31 |  5280
 2019-11-01 |  5309
 2019-11-02 |  1893
 2019-11-05 |  1773
 2019-11-06 |  5344
 2019-11-07 |  5488
(22 rows)

So search_results_html

    date    | count
------------+-------
 2019-10-14 |  3976
 2019-10-15 |  4824
 2019-10-17 |   224
 2019-10-18 |   113
 2019-10-19 |  1988
 2019-10-20 |  5112
 2019-10-21 |  5092
 2019-10-22 |  3535
 2019-10-23 |   342
 2019-10-24 |   321
 2019-10-25 |   347
 2019-10-26 |   337
 2019-10-27 |  3278
 2019-10-28 |  4722
 2019-10-29 |  5057
 2019-10-30 |  5194
 2019-10-31 |  5280
 2019-11-01 |  5309
 2019-11-02 |  1893
 2019-11-05 |  1773
 2019-11-06 |  5344
 2019-11-07 |  5488

 Uai, aqui deu a mesma coisa. Pq o left inclusive é diferente?




 Vejamos o LEFT EXCLUSIVE na search_results_db

SELECT count(*)
FROM search_results_db as d
LEFT OUTER JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
   h.account = d.account and 
   h.query = d.query and
   h.position = d.position and
   h.url = d.url
WHERE h.id IS NULL

 count
-------
 13813
(1 row)

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as date, count(*) 
from (
    SELECT d.timestamp, d.account, d.query, d.position, d.url
    FROM search_results_db as d
    LEFT OUTER JOIN search_results_html as h
    ON h.timestamp = d.timestamp and 
       h.account = d.account and 
       h.query = d.query and
       h.position = d.position and
       h.url = d.url
    WHERE h.id IS NULL
    ) as le
group by date order by date;

    date    | count
------------+-------
 2019-10-25 |     8
 2019-10-26 |   144
 2019-10-27 |  1361
 2019-10-28 |  2954
 2019-10-29 |  1859
 2019-10-30 |  1640
 2019-10-31 |  1351
 2019-11-01 |  1366
 2019-11-02 |   365
 2019-11-05 |   566
 2019-11-06 |  1282
 2019-11-07 |   899
            |    18
(13 rows)


# Nao consegui fazer esta query funcionar. Pq sera?

-- select to_char(timestamp :: date, 'yyyy-mm-dd') as date1
-- from (
--     SELECT d.timestamp
--     FROM search_results_db as d
--     LEFT OUTER JOIN search_results_html as h
--     ON h.timestamp = d.timestamp and 
--        h.account = d.account and 
--        h.query = d.query and
--        h.position = d.position and
--        h.url = d.url
--     WHERE h.id IS NULL 
--     ) as le
-- where date1 = '2019-10-25'
-- group by date1 order by date1;

E o RIGHT EXCLUSIVE na search_results_html

SELECT count(*)
FROM search_results_db as d
RIGHT OUTER JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
   h.account = d.account and 
   h.query = d.query and
   h.position = d.position and
   h.url = d.url
WHERE d.id IS NULL

 count
-------
 34848
(1 row)

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as date, count(*) 
from (
    SELECT h.timestamp, h.account, h.query, h.position, h.url
    FROM search_results_db as d
    RIGHT OUTER JOIN search_results_html as h
    ON h.timestamp = d.timestamp and 
       h.account = d.account and 
       h.query = d.query and
       h.position = d.position and
       h.url = d.url
    WHERE d.id IS NULL) as re
group by date order by date;

    date    | count
------------+-------
 2019-10-14 |  3976
 2019-10-15 |  4824
 2019-10-17 |   224
 2019-10-18 |   113
 2019-10-19 |  1988
 2019-10-20 |  5112
 2019-10-21 |  5092
 2019-10-22 |  3535
 2019-10-23 |   342
 2019-10-24 |   321
 2019-10-25 |   338
 2019-10-26 |   110
 2019-10-27 |   994
 2019-10-28 |  2186
 2019-10-29 |  1201
 2019-10-30 |  1031
 2019-10-31 |   848
 2019-11-01 |   864
 2019-11-02 |   206
 2019-11-05 |   359
 2019-11-06 |   753
 2019-11-07 |   431
(22 rows)

select distinct(to_char(timestamp :: date, 'yyyy-mm-dd')) as date, count(*) 
from (
    SELECT h.timestamp, h.account, h.query, h.position, h.url
    FROM search_results_db as d
    INNER JOIN search_results_html as h
    ON h.timestamp = d.timestamp and 
       h.account = d.account and 
       h.query = d.query and
       h.position = d.position and
       h.url = d.url) as i
group by date order by date;

    date    | count
------------+-------
 2019-10-25 |     9
 2019-10-26 |   227
 2019-10-27 |  2284
 2019-10-28 |  2536
 2019-10-29 |  3856
 2019-10-30 |  4163
 2019-10-31 |  4432
 2019-11-01 |  4445
 2019-11-02 |  1687
 2019-11-05 |  1414
 2019-11-06 |  4591
 2019-11-07 |  5057
(12 rows)












# verificando um deles

select h.id, d.id, h.timestamp, d.timestamp, h.account, d.account, h.query, d.query, h.position, d.position
from search_results_html as h,
     search_results_db as d
where h.id = 62218 and d.id = 49;

select h.id, d.id, h.timestamp, d.timestamp, h.account, d.account, h.query, d.query, h.position, d.position, h.url, d.url
from search_results_html as h,
     search_results_db as d
where h.id = 62218 and d.id = 49;

# linhas que estao em _html mas não em _db

select id, timestamp, account, query, position 
from search_results_db 
except 
select id, timestamp, account, query, position 
from search_results_html;

# quantas linhas?

select count(*) from search_results_db;
 count
-------
 48514
(1 row)

select count(*) from search_results_html;
 count
-------
 69549
(1 row)

-- Tudo fudado
--
-- select count(*) from (
-- select timestamp, account, query, position 
-- from search_results_db 
-- except 
-- select timestamp, account, query, position 
-- from search_results_html
-- ) as t;

--  count
-- -------
--   4855

-- select count(*) from (
-- select timestamp, account, query, position 
-- from search_results_html
-- except 
-- select timestamp, account, query, position 
-- from search_results_db
-- ) as t;

--  count
-- -------
--  25890
-- (1 row)


Outras estratégias, pra conferir...

# Linhas exclusivas de search_results_db

SELECT count(*)
FROM   search_results_db as d
WHERE  NOT EXISTS (SELECT 1 FROM search_results_html as h 
    WHERE h.timestamp = d.timestamp and 
      h.account = d.account and 
      h.query = d.query and
      h.position = d.position and
      h.url = d.url);

 count
-------
 13813
(1 row)

# Linhas exclusivas de search_results_html

SELECT count(*)
FROM   search_results_html as d
WHERE  NOT EXISTS (SELECT 1 FROM search_results_db as h 
    WHERE h.timestamp = d.timestamp and 
      h.account = d.account and 
      h.query = d.query and
      h.position = d.position and
      h.url = d.url);

 count
-------
 34848
(1 row)


-- SELECT d.id, h.id, d.timestamp, d.account, d.query, d.position
-- FROM search_results_db as d
-- RIGHT JOIN search_results_html as h
-- ON h.timestamp = d.timestamp and 
--    h.account = d.account and 
--    h.query = d.query and
--    h.position = d.position and
--    h.url = d.url

-- SELECT d.id, h.id, h.timestamp, h.account, h.query, h.position
-- FROM search_results_db as d
-- RIGHT JOIN search_results_html as h
-- ON h.timestamp = d.timestamp and 
--    h.account = d.account and 
--    h.query = d.query and
--    h.position = d.position and
--    h.url = d.url

SELECT count(*)
FROM search_results_db as d
RIGHT JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
      h.account = d.account and 
      h.query = d.query and
      h.position = d.position and
      h.url = d.url
WHERE d.id is null

 count
-------
 34848
(1 row)

# O que é isso?

SELECT count(*)
FROM search_results_db as d
RIGHT JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
      h.account = d.account and 
      h.query = d.query and
      h.position = d.position and
      h.url = d.url
WHERE d.id is not null

 count
-------
 34701
(1 row)

# 
SELECT count(*)
FROM search_results_db as d
LEFT JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
      h.account = d.account and 
      h.query = d.query and
      h.position = d.position and
      h.url = d.url
WHERE d.id is null

 count
-------
     0
(1 row)

SELECT count(*)
FROM search_results_db as d
LEFT JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
      h.account = d.account and 
      h.query = d.query and
      h.position = d.position and
      h.url = d.url
WHERE d.id is not null

 count
-------
 48514
(1 row)


SELECT count(*) 
FROM search_results_db as d
FULL OUTER JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
   h.account = d.account and 
   h.query = d.query and
   h.position = d.position and
   h.url = d.url;

 count
-------
 83362
(1 row)

SELECT count(*) 
FROM search_results_db as d
FULL OUTER JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
   h.account = d.account and 
   h.query = d.query and
   h.position = d.position and
   h.url = d.url
WHERE d.id IS NULL or 
      h.id IS NULL;

 count
-------
 48661
(1 row)



-------------------------

# Left inclusive 

SELECT count(*) 
FROM search_results_db as d
LEFT JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
   h.account = d.account and 
   h.query = d.query and
   h.position = d.position and
   h.url = d.url;

 count
-------
 48514        # mesma quantidade de linhas do que search_results_db
(1 row)

# mesma coisa, com OUTER 

SELECT count(*) 
FROM search_results_db as d
LEFT OUTER JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
   h.account = d.account and 
   h.query = d.query and
   h.position = d.position and
   h.url = d.url;

 count
-------
 48514
(1 row)


# Right inclusive

SELECT count(*) 
FROM search_results_db as d
RIGHT JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
   h.account = d.account and 
   h.query = d.query and
   h.position = d.position and
   h.url = d.url;

 count
-------
 69549
(1 row)

# mesma coisa, com OUTER

SELECT count(*) 
FROM search_results_db as d
RIGHT OUTER JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
   h.account = d.account and 
   h.query = d.query and
   h.position = d.position and
   h.url = d.url;
 
 count
-------
 69549
(1 row)



SELECT count(*) 
FROM search_results_db as d
LEFT OUTER JOIN search_results_html as h
ON h.timestamp = d.timestamp and 
   h.account = d.account and 
   h.query = d.query and
   h.position = d.position and
   h.url = d.url
WHERE d.id IS NULL;

 count
-------
     0
(1 row)


conferir contagem do inner, left e right (inclusive e exclusive)


SELECT d.id, h.id, d.timestamp, d.account, d.query, d.position
FROM   search_results_db as d
FULL   OUTER JOIN search_results_html as h USING (timestamp, account, query, position)
WHERE  d.id IS NULL OR
       tbl1.col IS NULL;







delete from search_results where search_results.id not in
    (select s2.id from
        (
            select distinct on (timestamp, account, query, position, url) * from search_results
        ) as s2
    );


# TODO

ids duplicados!!! Adicionar UNIQUE
underscore no nome dos htmls
retirar id dos csvs