-- CREATE TABLE resultados(
--     data int
-- )

SELECT 'CREATE DATABASE googlescraper'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'googlescraper')\gexec 

SELECT 'CREATE DATABASE googlescraper_dev'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'googlescraper_dev')\gexec

-- CREATE USER postgres with encrypted password 'resocie123';

GRANT ALL PRIVILEGES ON DATABASE googlescraper TO postgres;
GRANT ALL PRIVILEGES ON DATABASE googlescraper_dev TO postgres;

-- GRANT permissions ON DATABASE dbname TO username;
